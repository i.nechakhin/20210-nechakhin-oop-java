package ru.ccfit.inechakhin.pacman.Controller;

import ru.ccfit.inechakhin.pacman.Model.Ghost;
import ru.ccfit.inechakhin.pacman.Model.Maze;
import ru.ccfit.inechakhin.pacman.Model.Pacman;
import ru.ccfit.inechakhin.pacman.View.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.PrintWriter;

public class OnlineController extends JPanel implements ActionListener {
    private boolean ghostInGame = false;
    private boolean pacmanInGame = false;
    private boolean dying = false;

    private int pacmanReqX, pacmanReqY;
    private int pacmanDx, pacmanDy;
    private int ghostReqX, ghostReqY;
    private int ghostDx, ghostDy;

    private boolean isServer;
    private PrintWriter output;

    Pacman pacman = new Pacman();
    Ghost ghost = new Ghost();
    Maze maze = new Maze();

    JFrame frame;

    public OnlineController (JFrame frame) {
        this.frame = frame;
        Timer timer = new Timer(50, this);
        timer.start();
        frame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed (KeyEvent e) {
                controllerKeyPressed(e);
            }
        });
        frame.setFocusable(true);
        initGame();
    }

    private void initGame () {
        ghost.setN_GHOSTS(1);
        pacman.setLives(3);
        pacman.setScore(0);
        initLevel();
    }

    private void initLevel () {
        maze.setStartScreenData();
        continueLevel();
    }

    private void continueLevel () {
        for (int i = 0; i < ghost.getN_GHOSTS(); ++i) {
            ghost.setElemGhostX(i, (12 + (i % 2)) * maze.getBLOCK_SIZE()); //start position
            ghost.setElemGhostY(i, 11 * maze.getBLOCK_SIZE());
            ghost.setElemGhostSpeed(i, ghost.getElemValidSpeeds(4));
        }
        pacman.setPacmanX(13 * maze.getBLOCK_SIZE()); //start position
        pacman.setPacmanY(22 * maze.getBLOCK_SIZE());
        dying = false;
    }

    private void death () {
        pacman.setLives(pacman.getLives() - 1);
        if (pacman.getLives() == 0 && pacmanInGame) {
            if (!isServer) {
                pacmanInGame = false;
                output.println(0 + "." + 0);
            }
        }
        continueLevel();
    }

    private void checkControllerActivity (View view) {
        if (pacmanReqX == -1 && pacmanReqY == 0) {
            view.drawLeft(pacman.getPacmanX() + 1, pacman.getPacmanY() + 1);
        } else if (pacmanReqX == 1 && pacmanReqY == 0) {
            view.drawRight(pacman.getPacmanX() + 1, pacman.getPacmanY() + 1);
        } else if (pacmanReqX == 0 && pacmanReqY == -1) {
            view.drawUp(pacman.getPacmanX() + 1, pacman.getPacmanY() + 1);
        } else if (pacmanReqX == 0 && pacmanReqY == 1) {
            view.drawDown(pacman.getPacmanX() + 1, pacman.getPacmanY() + 1);
        } else if (pacmanReqX == 0 && pacmanReqY == 0) {
            pacmanInGame = false;
        } else if (pacmanReqX == 1 && pacmanReqY == 1) {
            pacmanInGame = true;
            view.drawRight(pacman.getPacmanX() + 1, pacman.getPacmanY() + 1);
            view.drawGhost(ghost.getElemGhostX(0) + 1, ghost.getElemGhostY(0) + 1);
            if (pacmanInGame && ghostInGame) {
                pacmanReqX = 1; pacmanReqY = 0;
                ghostReqX = 0; ghostReqY = -1;
                checkControllerActivity(view);
                initGame();
            }
        }
        if (ghostReqX == -1 && ghostReqY == 0 || ghostReqX == 1 && ghostReqY == 0
                || ghostReqX == 0 && ghostReqY == -1 || ghostReqX == 0 && ghostReqY == 1) {
            view.drawGhost(ghost.getElemGhostX(0) + 1, ghost.getElemGhostY(0) + 1);
        } else if (ghostReqX == 0 && ghostReqY == 0) {
            ghostInGame = false;
        } else if (ghostReqX == 1 && ghostReqY == 1) {
            ghostInGame = true;
            view.drawRight(pacman.getPacmanX() + 1, pacman.getPacmanY() + 1);
            view.drawGhost(ghost.getElemGhostX(0) + 1, ghost.getElemGhostY(0) + 1);
            if (pacmanInGame && ghostInGame) {
                pacmanReqX = 1; pacmanReqY = 0;
                ghostReqX = 0; ghostReqY = -1;
                checkControllerActivity(view);
                initGame();
            }
        }
        if (!pacmanInGame || !ghostInGame) {
            death();
        }
    }

    private void moveGhosts () {
        boolean isSkipMove = false;
        if (ghost.getElemGhostX(0) % maze.getBLOCK_SIZE() == 0 && ghost.getElemGhostY(0) % maze.getBLOCK_SIZE() == 0) {
            int pos = ghost.getElemGhostX(0) / maze.getBLOCK_SIZE() + maze.getNX_BLOCKS() * (ghost.getElemGhostY(0) / maze.getBLOCK_SIZE());
            short ch = maze.getElemScreenData(pos);

            if (ghostReqX != 0 || ghostReqY != 0) {
                if (!((ghostReqX == -1 && ghostReqY == 0 && (ch & 1) != 0)
                        || (ghostReqX == 1 && ghostReqY == 0 && (ch & 4) != 0)
                        || (ghostReqX == 0 && ghostReqY == -1 && (ch & 2) != 0)
                        || (ghostReqX == 0 && ghostReqY == 1 && (ch & 8) != 0))) {
                    ghostDx = ghostReqX;
                    ghostDy = ghostReqY;
                } else {
                    // standstill
                    ghostDx = 0;
                    ghostDy = 0;
                }
            }
            if (ghost.getElemGhostY(0) / maze.getBLOCK_SIZE() == 13) {
                if (ghost.getElemGhostX(0) / maze.getBLOCK_SIZE() == 25 && ghostDx == 1) {
                    ghost.setElemGhostX(0, 0);
                    isSkipMove = true;
                }
                if (ghost.getElemGhostX(0) / maze.getBLOCK_SIZE() == 0 && ghostDx == -1) {
                    ghost.setElemGhostX(0, 25 * maze.getBLOCK_SIZE());
                    isSkipMove = true;
                }
            }
        }
        if (!isSkipMove) {
            ghost.setElemGhostX(0, ghost.getElemGhostX(0) + ghost.getElemGhostSpeed(0) * ghostDx);
            ghost.setElemGhostY(0, ghost.getElemGhostY(0) + ghost.getElemGhostSpeed(0) * ghostDy);
        }
        if (pacman.getPacmanX() > (ghost.getElemGhostX(0) - (maze.getBLOCK_SIZE() / 2))
                && pacman.getPacmanX() < (ghost.getElemGhostX(0) + (maze.getBLOCK_SIZE() / 2))
                && pacman.getPacmanY() > (ghost.getElemGhostY(0) - (maze.getBLOCK_SIZE() / 2))
                && pacman.getPacmanY() < (ghost.getElemGhostY(0) + (maze.getBLOCK_SIZE() / 2))
                && pacmanInGame && ghostInGame) {
            dying = true;
        }
    }

    private void checkMaze () {
        if (maze.isFinished()) {
            pacman.setScore(pacman.getScore() + 50);
            if (ghost.getCurrentSpeed() < ghost.getMaxSpeed()) {
                ghost.setCurrentSpeed(ghost.getCurrentSpeed() + 1);
            }
            initLevel();
        }
    }

    private void playGame (Graphics g) {
        if (dying) {
            death();
        } else {
            View view = new View(g);
            checkControllerActivity(view);
            checkMaze();
        }
    }

    public void movePacman () {
        if (pacman.getPacmanX() % maze.getBLOCK_SIZE() == 0 && pacman.getPacmanY() % maze.getBLOCK_SIZE() == 0) {
            int pos = pacman.getPacmanX() / maze.getBLOCK_SIZE() + maze.getNX_BLOCKS() * (pacman.getPacmanY() / maze.getBLOCK_SIZE());
            short ch = maze.getElemScreenData(pos);

            if ((ch & 16) != 0) {
                maze.setElemScreenData(pos, (short) (ch & 15));
                if (maze.getElemScreenData(pos) == 0) {
                    maze.setElemScreenData(pos, (short) 32);
                }
                pacman.setScore(pacman.getScore() + 1);
            }

            if (pacmanReqX != 0 || pacmanReqY != 0) {
                if (!((pacmanReqX == -1 && pacmanReqY == 0 && (ch & 1) != 0)
                        || (pacmanReqX == 1 && pacmanReqY == 0 && (ch & 4) != 0)
                        || (pacmanReqX == 0 && pacmanReqY == -1 && (ch & 2) != 0)
                        || (pacmanReqX == 0 && pacmanReqY == 1 && (ch & 8) != 0))) {
                    pacmanDx = pacmanReqX;
                    pacmanDy = pacmanReqY;
                } else {
                    // standstill
                    pacmanDx = 0;
                    pacmanDy = 0;
                }
            }
            if (pacman.getPacmanY() / maze.getBLOCK_SIZE() == 13) {
                if (pacman.getPacmanX() / maze.getBLOCK_SIZE() == 25 && pacmanDx == 1) {
                    pacman.setPacmanX(0);
                    return;
                }
                if (pacman.getPacmanX() / maze.getBLOCK_SIZE() == 0 && pacmanDx == -1) {
                    pacman.setPacmanX(25 * maze.getBLOCK_SIZE());
                    return;
                }
            }
        }
        pacman.setPacmanX(pacman.getPacmanX() + pacman.getPacmanSpeed() * pacmanDx);
        pacman.setPacmanY(pacman.getPacmanY() + pacman.getPacmanSpeed() * pacmanDy);
    }

    public void controllerKeyPressed (KeyEvent e) {
        int key = e.getKeyCode();
        if (key == KeyEvent.VK_LEFT) {
            if (isServer) {
                ghostReqX = -1;
                ghostReqY = 0;
                output.println(ghostReqX + "." + ghostReqY);
            } else {
                pacmanReqX = -1;
                pacmanReqY = 0;
                output.println(pacmanReqX + "." + pacmanReqY);
            }
        } else if (key == KeyEvent.VK_RIGHT) {
            if (isServer) {
                ghostReqX = 1;
                ghostReqY = 0;
                output.println(ghostReqX + "." + ghostReqY);
            } else {
                pacmanReqX = 1;
                pacmanReqY = 0;
                output.println(pacmanReqX + "." + pacmanReqY);
            }
        } else if (key == KeyEvent.VK_UP) {
            if (isServer) {
                ghostReqX = 0;
                ghostReqY = -1;
                output.println(ghostReqX + "." + ghostReqY);
            } else {
                pacmanReqX = 0;
                pacmanReqY = -1;
                output.println(pacmanReqX + "." + pacmanReqY);
            }
        } else if (key == KeyEvent.VK_DOWN) {
            if (isServer) {
                ghostReqX = 0;
                ghostReqY = 1;
                output.println(ghostReqX + "." + ghostReqY);
            } else {
                pacmanReqX = 0;
                pacmanReqY = 1;
                output.println(pacmanReqX + "." + pacmanReqY);
            }
        } else if (key == KeyEvent.VK_ESCAPE) {
            if (isServer) {
                ghostReqX = 0;
                ghostReqY = 0;
                output.println(ghostReqX + "." + ghostReqY);
            } else {
                pacmanReqX = 0;
                pacmanReqY = 0;
                output.println(pacmanReqX + "." + pacmanReqY);
            }
        } else if (key == KeyEvent.VK_SPACE) {
            if (isServer) {
                ghostReqX = 1;
                ghostReqY = 1;
                output.println(ghostReqX + "." + ghostReqY);
            } else {
                pacmanReqX = 1;
                pacmanReqY = 1;
                output.println(pacmanReqX + "." + pacmanReqY);
            }
        }
    }

    public void paintComponent (Graphics g) {
        View view = new View(g);
        view.drawMaze(maze.getScreenData());
        if (!isServer) {
            view.drawScore(pacman.getScore());
            view.drawHearts(pacman.getLives());
        }
        checkControllerActivity(view);
        if (pacmanInGame && ghostInGame) {
            playGame(g);
        } else {
            if (isServer && !ghostInGame || !isServer && !pacmanInGame) {
                view.showIntroScreen();
            }
        }
        Toolkit.getDefaultToolkit().sync();
        g.dispose();
    }

    @Override
    public void actionPerformed (ActionEvent e) {
        frame.repaint();
        if (pacmanInGame && ghostInGame && !dying) {
            moveGhosts();
            movePacman();
        }
    }

    public void setIsServer (boolean isServer) {
        this.isServer = isServer;
    }

    public void setOutput (PrintWriter output) {
        this.output = output;
    }

    public void setPacmanReqX (int pacmanReqX) {
        this.pacmanReqX = pacmanReqX;
    }

    public void setPacmanReqY (int pacmanReqY) {
        this.pacmanReqY = pacmanReqY;
    }

    public void setGhostReqX (int ghostReqX) {
        this.ghostReqX = ghostReqX;
    }

    public void setGhostReqY (int ghostReqY) {
        this.ghostReqY = ghostReqY;
    }
}

