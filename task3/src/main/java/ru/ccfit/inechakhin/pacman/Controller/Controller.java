package ru.ccfit.inechakhin.pacman.Controller;

import ru.ccfit.inechakhin.pacman.Model.*;
import ru.ccfit.inechakhin.pacman.View.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Controller extends JPanel implements ActionListener {
    private boolean inGame = false;
    private boolean dying = false;

    private int reqX, reqY;
    private int pacmanDx, pacmanDy;

    Pacman pacman = new Pacman();
    Ghost ghost = new Ghost();
    Maze maze = new Maze();

    JFrame frame;

    public Controller (JFrame frame) {
        this.frame = frame;
        Timer timer = new Timer(50, this);
        timer.start();
        frame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed (KeyEvent e) {
                controllerKeyPressed(e);
            }
        });
        frame.setFocusable(true);
        initGame();
    }

    private void initGame () {
        pacman.setLives(3);
        pacman.setScore(0);
        initLevel();
    }

    private void initLevel () {
        maze.setStartScreenData();
        continueLevel();
    }

    private void continueLevel () {
        int random;
        for (int i = 0; i < ghost.getN_GHOSTS(); ++i) {
            ghost.setElemGhostX(i, (12 + (i % 2)) * maze.getBLOCK_SIZE()); //start position
            ghost.setElemGhostY(i, 11 * maze.getBLOCK_SIZE());
            ghost.setElemGhostDx(i, 0);
            ghost.setElemGhostDy(i, -1);
            random = (int) (Math.random() * ghost.getMaxSpeed());
            if (random > ghost.getCurrentSpeed()) {
                random = ghost.getCurrentSpeed();
            }
            ghost.setElemGhostSpeed(i, ghost.getElemValidSpeeds(random));
        }
        pacman.setPacmanX(13 * maze.getBLOCK_SIZE()); //start position
        pacman.setPacmanY(22 * maze.getBLOCK_SIZE());
        dying = false;
    }

    private void death () {
        pacman.setLives(pacman.getLives() - 1);
        if (pacman.getLives() == 0) {
            inGame = false;
        }
        continueLevel();
    }

    private void checkControllerActivity (View view) {
        if (reqX == -1 && reqY == 0) {
            view.drawLeft(pacman.getPacmanX() + 1, pacman.getPacmanY() + 1);
        } else if (reqX == 1 && reqY == 0) {
            view.drawRight(pacman.getPacmanX() + 1, pacman.getPacmanY() + 1);
        } else if (reqX == 0 && reqY == -1) {
            view.drawUp(pacman.getPacmanX() + 1, pacman.getPacmanY() + 1);
        } else if (reqX == 0 && reqY == 1) {
            view.drawDown(pacman.getPacmanX() + 1, pacman.getPacmanY() + 1);
        } else if (reqX == 0 && reqY == 0) {
            inGame = false;
            death();
        } else if (reqX == 1 && reqY == 1) {
            inGame = true;
            initGame();
            reqX = 1; reqY = 0;
            checkControllerActivity(view);
        }
    }

    private void moveGhosts (View view) {
        boolean isSkipMove = false;
        int pos;
        int count;
        for (int i = 0; i < ghost.getN_GHOSTS(); ++i) {
            if (ghost.getElemGhostX(i) % maze.getBLOCK_SIZE() == 0 && ghost.getElemGhostY(i) % maze.getBLOCK_SIZE() == 0) {
                pos = ghost.getElemGhostX(i) / maze.getBLOCK_SIZE() + maze.getNX_BLOCKS() * (ghost.getElemGhostY(i) / maze.getBLOCK_SIZE());
                count = 0;
                if ((maze.getElemScreenData(pos) & 1) == 0 && ghost.getElemGhostDx(i) != 1) {
                    ghost.setElemDx(count, -1);
                    ghost.setElemDy(count, 0);
                    count++;
                }
                if ((maze.getElemScreenData(pos) & 2) == 0 && ghost.getElemGhostDy(i) != 1) {
                    ghost.setElemDx(count, 0);
                    ghost.setElemDy(count, -1);
                    count++;
                }
                if ((maze.getElemScreenData(pos) & 4) == 0 && ghost.getElemGhostDx(i) != -1) {
                    ghost.setElemDx(count, 1);
                    ghost.setElemDy(count, 0);
                    count++;
                }
                if ((maze.getElemScreenData(pos) & 8) == 0 && ghost.getElemGhostDy(i) != -1) {
                    ghost.setElemDx(count, 0);
                    ghost.setElemDy(count, 1);
                    count++;
                }
                if (count == 0) {
                    if ((maze.getElemScreenData(pos) & 15) == 15) {
                        ghost.setElemGhostDx(i, 0);
                        ghost.setElemGhostDy(i, 0);
                    } else {
                        ghost.setElemGhostDx(i, -ghost.getElemGhostDx(i));
                        ghost.setElemGhostDy(i, -ghost.getElemGhostDy(i));
                    }
                } else {
                    count = (int) (Math.random() * count);
                    if (count > 3) {
                        count = 3;
                    }
                    ghost.setElemGhostDx(i, ghost.getElemDx(count));
                    ghost.setElemGhostDy(i, ghost.getElemDy(count));
                }
                if (ghost.getElemGhostY(i) / maze.getBLOCK_SIZE()== 13) {
                    if (ghost.getElemGhostX(i) / maze.getBLOCK_SIZE() == 25 && ghost.getElemGhostDx(i) == 1) {
                        ghost.setElemGhostX(i, 0);
                        isSkipMove = true;
                    }
                    if (ghost.getElemGhostX(i) / maze.getBLOCK_SIZE() == 0 && ghost.getElemGhostDx(i) == -1) {
                        ghost.setElemGhostX(i, 25 * maze.getBLOCK_SIZE());
                        isSkipMove = true;
                    }
                }
            }

            if (!isSkipMove) {
                ghost.setElemGhostX(i, ghost.getElemGhostX(i) + (ghost.getElemGhostDx(i) * ghost.getElemGhostSpeed(i)));
                ghost.setElemGhostY(i, ghost.getElemGhostY(i) + (ghost.getElemGhostDy(i) * ghost.getElemGhostSpeed(i)));
            }

            view.drawGhost(ghost.getElemGhostX(i) + 1, ghost.getElemGhostY(i) + 1);

            if (pacman.getPacmanX() > (ghost.getElemGhostX(i) - (maze.getBLOCK_SIZE() / 2))
                    && pacman.getPacmanX()  < (ghost.getElemGhostX(i) + (maze.getBLOCK_SIZE() / 2))
                    && pacman.getPacmanY()  > (ghost.getElemGhostY(i) - (maze.getBLOCK_SIZE() / 2))
                    && pacman.getPacmanY()  < (ghost.getElemGhostY(i) + (maze.getBLOCK_SIZE() / 2))
                    && inGame) {
                dying = true;
            }
        }
    }

    private void checkMaze () {
        if (maze.isFinished()) {
            pacman.setScore(pacman.getScore() + 50);
            if (ghost.getCurrentSpeed() < ghost.getMaxSpeed()) {
                ghost.setCurrentSpeed(ghost.getCurrentSpeed() + 1);
            }
            initLevel();
        }
    }

    private void playGame (Graphics g) {
        if (dying) {
            death();
        } else {
            View view = new View(g);
            checkControllerActivity(view);
            moveGhosts(view);
            checkMaze();
        }
    }

    public void movePacman () {
        if (pacman.getPacmanX() % maze.getBLOCK_SIZE() == 0 && pacman.getPacmanY() % maze.getBLOCK_SIZE() == 0) {
            int pos = pacman.getPacmanX()  / maze.getBLOCK_SIZE() + maze.getNX_BLOCKS() * (pacman.getPacmanY() / maze.getBLOCK_SIZE());
            short ch = maze.getElemScreenData(pos);

            if ((ch & 16) != 0) {
                maze.setElemScreenData(pos, (short) (ch & 15));
                if (maze.getElemScreenData(pos) == 0) {
                    maze.setElemScreenData(pos, (short) 32);
                }
                pacman.setScore(pacman.getScore() + 1);
            }

            if (reqX != 0 || reqY != 0) {
                if (!((reqX == -1 && reqY == 0 && (ch & 1) != 0)
                        || (reqX == 1 && reqY == 0 && (ch & 4) != 0)
                        || (reqX == 0 && reqY == -1 && (ch & 2) != 0)
                        || (reqX == 0 && reqY == 1 && (ch & 8) != 0))) {
                    pacmanDx = reqX;
                    pacmanDy = reqY;
                } else {
                    // standstill
                    pacmanDx = 0;
                    pacmanDy = 0;
                }
            }
            if (pacman.getPacmanY() / maze.getBLOCK_SIZE() == 13) {
                if (pacman.getPacmanX() / maze.getBLOCK_SIZE() == 25 && pacmanDx == 1) {
                    pacman.setPacmanX(0);
                    return;
                }
                if (pacman.getPacmanX() / maze.getBLOCK_SIZE() == 0 && pacmanDx == -1) {
                    pacman.setPacmanX(25 * maze.getBLOCK_SIZE());
                    return;
                }
            }
        }
        pacman.setPacmanX(pacman.getPacmanX() + pacman.getPacmanSpeed() * pacmanDx);
        pacman.setPacmanY(pacman.getPacmanY() + pacman.getPacmanSpeed() * pacmanDy);
    }

    public void controllerKeyPressed (KeyEvent e) {
        int key = e.getKeyCode();
        if (key == KeyEvent.VK_LEFT) {
            reqX = -1;
            reqY = 0;
        } else if (key == KeyEvent.VK_RIGHT) {
            reqX = 1;
            reqY = 0;
        } else if (key == KeyEvent.VK_UP) {
            reqX = 0;
            reqY = -1;
        } else if (key == KeyEvent.VK_DOWN) {
            reqX = 0;
            reqY = 1;
        } else if (key == KeyEvent.VK_ESCAPE) {
            reqX = 0;
            reqY = 0;
        } else if (key == KeyEvent.VK_SPACE) {
            reqX = 1;
            reqY = 1;
        }
    }

    public void paintComponent (Graphics g) {
        View view = new View(g);
        view.drawMaze(maze.getScreenData());
        view.drawScore(pacman.getScore());
        view.drawHearts(pacman.getLives());
        checkControllerActivity(view);
        if (inGame) {
            playGame(g);
        } else {
            view.showIntroScreen();
        }
        Toolkit.getDefaultToolkit().sync();
        g.dispose();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        frame.repaint();
        if (inGame && !dying) {
            movePacman();
        }
    }
}
