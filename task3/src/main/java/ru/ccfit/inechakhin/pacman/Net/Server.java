package ru.ccfit.inechakhin.pacman.Net;

import ru.ccfit.inechakhin.pacman.Controller.OnlineController;

import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    OnlineController controller;
    JFrame frame;

    PrintWriter output;

    private Socket socket;

    private void initFrame () {
        frame = new JFrame("Server");
        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                output.println("Exit");
                frame.dispose();
                System.exit(0);
            }
        });
        frame.setSize(624, 750);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        this.controller = new OnlineController(frame);
        frame.add(controller);
        frame.setVisible(true);
    }

    public Server () {
        try {
            ServerSocket listener = new ServerSocket(3000);
            initFrame();
            this.socket = listener.accept();
            setPositions();
            frame.dispose();
            System.exit(0);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.getStackTrace();
        }
    }

    private void setPositions () {
        try {
            controller.setIsServer(true);
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(socket.getOutputStream(), true);
            controller.setOutput(output);
            while (true) {
                String command = input.readLine();
                if (command.equals("Exit")) {
                    break;
                }
                controller.setPacmanReqX(Integer.parseInt(command.substring(0, command.lastIndexOf("."))));
                controller.setPacmanReqY(Integer.parseInt(command.substring(command.lastIndexOf(".") + 1)));
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.getStackTrace();
        }
    }

    public static void main (String[] args) {
        new Server();
    }
}
