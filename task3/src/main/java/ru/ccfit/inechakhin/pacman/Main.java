package ru.ccfit.inechakhin.pacman;

import ru.ccfit.inechakhin.pacman.Controller.*;

import javax.swing.*;

public class Main {
    public static void main (String[] args) {
        JFrame frame = new JFrame("Pacman");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(624,750);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.add(new Controller(frame));
        frame.setVisible(true);
    }
}
