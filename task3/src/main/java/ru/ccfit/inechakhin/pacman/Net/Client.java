package ru.ccfit.inechakhin.pacman.Net;

import ru.ccfit.inechakhin.pacman.Controller.OnlineController;

import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.net.Socket;

public class Client {
    OnlineController controller;
    JFrame frame;

    PrintWriter output;

    private Socket socket;

    private void initFrame () {
        frame = new JFrame("Client");
        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                output.println("Exit");
                frame.dispose();
                System.exit(0);
            }
        });
        frame.setSize(624, 750);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        this.controller = new OnlineController(frame);
        frame.add(controller);
        frame.setVisible(true);
    }

    public Client (String serverAddress) throws Exception {
        try {
            this.socket = new Socket(serverAddress, 3000);
            initFrame();
            setPositions();
            if (socket != null) {
                socket.close();
            }
            frame.dispose();
            System.exit(0);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.getStackTrace();
        }
    }

    private void setPositions () {
        try {
            controller.setIsServer(false);
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(socket.getOutputStream(), true);
            controller.setOutput(output);
            while (true) {
                String command = input.readLine();
                if (command.equals("Exit")) {
                    break;
                }
                controller.setGhostReqX(Integer.parseInt(command.substring(0, command.lastIndexOf("."))));
                controller.setGhostReqY(Integer.parseInt(command.substring(command.lastIndexOf(".") + 1)));
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.getStackTrace();
        }
    }

    public static void main (String[] args) throws Exception {
        new Client("127.0.0.1");
    }
}
