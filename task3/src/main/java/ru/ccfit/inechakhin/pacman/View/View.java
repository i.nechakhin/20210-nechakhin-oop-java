package ru.ccfit.inechakhin.pacman.View;

import ru.ccfit.inechakhin.pacman.Model.*;

import javax.swing.*;
import java.awt.*;

public class View {
    private final int BLOCK_SIZE;
    private final int NX_BLOCKS;
    private final int NY_BLOCKS;
    private final int SCREEN_SIZE_X;
    private final int SCREEN_SIZE_Y;

    private Image heart, ghost, up, down, left, right;

    private final Font smallFont = new Font("Arial", Font.BOLD, 16);
    private final Graphics2D g2d;

    Maze maze = new Maze();

    public View (Graphics g) {
        loadImages();
        g2d = (Graphics2D) g;
        BLOCK_SIZE = maze.getBLOCK_SIZE();
        NX_BLOCKS = maze.getNX_BLOCKS();
        NY_BLOCKS = maze.getNY_BLOCKS();
        SCREEN_SIZE_X = NX_BLOCKS * BLOCK_SIZE;
        SCREEN_SIZE_Y = NY_BLOCKS * BLOCK_SIZE;
    }

    private void loadImages () {
        heart = new ImageIcon("../task3/src/main/resources/images/heart.png").getImage();
        down = new ImageIcon("../task3/src/main/resources/images/down.gif").getImage();
        up = new ImageIcon("../task3/src/main/resources/images/up.gif").getImage();
        left = new ImageIcon("../task3/src/main/resources/images/left.gif").getImage();
        right = new ImageIcon("../task3/src/main/resources/images/right.gif").getImage();
        ghost = new ImageIcon("../task3/src/main/resources/images/ghost.gif").getImage();
    }

    public void drawHeart (int startX, int startY) {
        g2d.drawImage(heart, startX, startY, null);
    }

    public void drawDown (int startX, int startY) {
        g2d.drawImage(down, startX, startY, null);
    }

    public void drawUp (int startX, int startY) {
        g2d.drawImage(up, startX, startY, null);
    }

    public void drawLeft (int startX, int startY) {
        g2d.drawImage(left, startX, startY, null);
    }

    public void drawRight (int startX, int startY) {
        g2d.drawImage(right, startX, startY, null);
    }

    public void drawGhost (int startX, int startY) {
        g2d.drawImage(ghost, startX, startY, null);
    }

    public void showIntroScreen () {
        g2d.setFont(smallFont);
        g2d.setColor(Color.yellow);
        g2d.drawString("Press SPACE to start", 9 * NX_BLOCKS, 13 * NY_BLOCKS);
    }

    public void drawScore (int score) {
        g2d.setFont(smallFont);
        g2d.setColor(new Color(5, 181, 79));
        g2d.drawString("Score: " + score, SCREEN_SIZE_X / 2 + 196, SCREEN_SIZE_Y + 16);
    }

    public void drawHearts (int lives) {
        for (int i = 0; i < lives; ++i) {
            drawHeart(i * 28 + 8, SCREEN_SIZE_Y + 1);
        }
    }

    public void drawMaze(short[] mazeData) {
        g2d.setColor(Color.black);
        g2d.fillRect(0, 0, 1800, 1800);
        int i = 0;
        for (int y = 0; y < SCREEN_SIZE_Y; y += BLOCK_SIZE) {
            for (int x = 0; x < SCREEN_SIZE_X; x += BLOCK_SIZE) {
                g2d.setColor(new Color(0,72,251));
                g2d.setStroke(new BasicStroke(5));
                if ((mazeData[i] == 0)) {
                    g2d.fillRect(x, y, BLOCK_SIZE, BLOCK_SIZE);
                }
                if ((mazeData[i] & 1) != 0) {
                    g2d.drawLine(x, y, x, y + BLOCK_SIZE - 1);
                }
                if ((mazeData[i] & 2) != 0) {
                    g2d.drawLine(x, y, x + BLOCK_SIZE - 1, y);
                }
                if ((mazeData[i] & 4) != 0) {
                    g2d.drawLine(x + BLOCK_SIZE - 1, y, x + BLOCK_SIZE - 1, y + BLOCK_SIZE - 1);
                }
                if ((mazeData[i] & 8) != 0) {
                    g2d.drawLine(x, y + BLOCK_SIZE - 1, x + BLOCK_SIZE - 1, y + BLOCK_SIZE - 1);
                }
                if ((mazeData[i] & 16) != 0) {
                    g2d.setColor(new Color(255,255,255));
                    g2d.fillOval(x + 10, y + 10, 6, 6);
                }
                i++;
            }
        }
    }
}
