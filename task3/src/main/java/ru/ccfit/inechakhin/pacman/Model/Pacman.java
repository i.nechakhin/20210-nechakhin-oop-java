package ru.ccfit.inechakhin.pacman.Model;

public class Pacman {
    private int pacmanX, pacmanY;
    private int lives, score;

    public int getPacmanX () {
        return pacmanX;
    }

    public void setPacmanX (int pacmanX) {
        this.pacmanX = pacmanX;
    }

    public int getPacmanY () {
        return pacmanY;
    }

    public void setPacmanY (int pacmanY) {
        this.pacmanY = pacmanY;
    }

    public int getPacmanSpeed () {
        return 6;
    }

    public int getLives () {
        return lives;
    }

    public void setLives (int lives) {
        this.lives = lives;
    }

    public int getScore () {
        return score;
    }

    public void setScore (int score) {
        this.score = score;
    }
}
