package ru.ccfit.inechakhin.pacman.Model;

public class Ghost {
    private int N_GHOSTS = 4;
    private final int[] ghostX = new int[N_GHOSTS];
    private final int[] ghostDx = new int[N_GHOSTS];
    private final int[] ghostY = new int[N_GHOSTS];
    private final int[] ghostDy = new int[N_GHOSTS];
    private final int[] ghostSpeed = new int[N_GHOSTS];
    private final int[] dx = new int[4];
    private final int[] dy = new int[4];

    private final int[] validSpeeds = {1, 2, 3, 4, 6, 8};
    private int currentSpeed = 3;

    public void setN_GHOSTS (int n_GHOSTS) {
        N_GHOSTS = n_GHOSTS;
    }

    public int getN_GHOSTS () {
        return N_GHOSTS;
    }

    public int getElemGhostX (int index) {
        return ghostX[index];
    }

    public void setElemGhostX (int index, int elem) {
        ghostX[index] = elem;
    }

    public int getElemGhostDx (int index) {
        return ghostDx[index];
    }

    public void setElemGhostDx (int index, int elem) {
        ghostDx[index] = elem;
    }

    public int getElemGhostY (int index) {
        return ghostY[index];
    }

    public void setElemGhostY (int index, int elem) {
        ghostY[index] = elem;
    }

    public int getElemGhostDy (int index) {
        return ghostDy[index];
    }

    public void setElemGhostDy (int index, int elem) {
        ghostDy[index] = elem;
    }

    public int getElemGhostSpeed (int index) {
        return ghostSpeed[index];
    }

    public void setElemGhostSpeed (int index, int elem) {
        ghostSpeed[index] = elem;
    }

    public int getElemDx (int index) {
        return dx[index];
    }

    public void setElemDx (int index, int elem) {
        dx[index] = elem;
    }

    public int getElemDy (int index) {
        return dy[index];
    }

    public void setElemDy (int index, int elem) {
        dy[index] = elem;
    }

    public int getElemValidSpeeds (int index) {
        return validSpeeds[index];
    }

    public int getMaxSpeed () {
        return 5;
    }

    public int getCurrentSpeed () {
        return currentSpeed;
    }

    public void setCurrentSpeed (int currentSpeed) {
        this.currentSpeed = currentSpeed;
    }
}
