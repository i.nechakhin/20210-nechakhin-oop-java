package ru.nsu.inechakhin;

import ru.nsu.inechakhin.factory.Factory;
import ru.nsu.inechakhin.view.View;

public class Main {
    public static void main(String[] args){
        Factory factory = Factory.getInstance();
        factory.getWorkPlan("/conf.properties");
        View.start(factory);
        factory.startWork();
    }
}
