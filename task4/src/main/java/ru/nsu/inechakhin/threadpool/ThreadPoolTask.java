package ru.nsu.inechakhin.threadpool;

public class ThreadPoolTask {
    private final TaskListener listener;
    private final Task task;

    public ThreadPoolTask (Task t, TaskListener l) {
        listener = l;
        task = t;
    }

    public void start () {
        listener.taskStarted(task);
    }

    public void finish () {
        listener.taskFinished(task);
    }

    public void interrupted () {
        listener.taskInterrupted(task);
    }

    public void go () throws InterruptedException {
        task.performWork();
    }

    public String getName () {
        return task.getName();
    }
}
