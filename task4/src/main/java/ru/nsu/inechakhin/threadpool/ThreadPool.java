package ru.nsu.inechakhin.threadpool;

import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class ThreadPool implements TaskListener {
    private static final Logger logger = Logger.getLogger(ThreadPool.class.getName());

    private final List<ThreadPoolTask> taskQueue = new LinkedList<>();

    public ThreadPool (int size, Executor[] execs) {
        for (int i = 0; i < size; ++i) {
            execs[i].addTaskQueue(taskQueue);
            new Thread(execs[i], "Executor#" + i).start();
        }
    }

    public void addTask (Task t) {
        synchronized (taskQueue) {
            taskQueue.add(new ThreadPoolTask(t, this));
            taskQueue.notify();
        }
    }

    public void taskStarted (Task t) {
        logger.info("STARTED THREAD > " + t.getName());
    }

    public void taskFinished (Task t) {
        logger.info("FINISHED THREAD > " + t.getName());
    }

    public void taskInterrupted (Task t) {
        logger.info("INTERRUPTED THREAD > " + t.getName());
    }

    public int getTasksNum () {
        return this.taskQueue.size();
    }
}
