package ru.nsu.inechakhin.threadpool;

public interface TaskListener {
    void taskInterrupted (Task t);

    void taskFinished (Task t);

    void taskStarted (Task t);
}
