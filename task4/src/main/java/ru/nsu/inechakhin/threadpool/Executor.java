package ru.nsu.inechakhin.threadpool;

import java.util.List;

public abstract class Executor implements Runnable {
    public abstract void addTaskQueue (List<ThreadPoolTask> list);

    public abstract void performTask (ThreadPoolTask t);
}
