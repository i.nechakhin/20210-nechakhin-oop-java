package ru.nsu.inechakhin.factory.roles;

import org.apache.log4j.Logger;
import ru.nsu.inechakhin.factory.products.Car;
import ru.nsu.inechakhin.factory.storages.ControlledStorage;

public class Dealer implements Runnable {
    private static final Logger logger = Logger.getLogger(Dealer.class.getName());

    private final ControlledStorage<Car> carStorage;

    // dealer parameters
    private final int dealerID;
    private static int sleepTimeMs;

    public Dealer (int ID, ControlledStorage<Car> carStorage, int startSleepTimeMs) {
        dealerID = ID;
        this.carStorage = carStorage;
        sleepTimeMs = startSleepTimeMs;

        logger.info("DEALER " + dealerID + " > CREATED");
    }

    @Override
    public void run () {
        logger.info("DEALER " + dealerID + " > START WORKING");

        while (true) {
            try {
                carStorage.addOrder();
                logger.info("DEALER " + dealerID + " > GETTING CAR...");
                Car car = carStorage.get();
                logger.info("DEALER " + dealerID + " > GOT CAR " + car.getID() + ", DETAILS: " + car.getEngineID() + ", " + car.getBodyID() + ", " + car.getAccessoryID());

                Thread.sleep(sleepTimeMs);
            } catch (InterruptedException e) {  
                logger.error(e.getMessage());
                e.printStackTrace();

                // finishing
                logger.info("DEALER " + dealerID + " > FINISHED WORKING");
                Thread.currentThread().interrupt();
            }
        }
    }

    public static void setTime (int time) {
        sleepTimeMs = time;
    }
}
