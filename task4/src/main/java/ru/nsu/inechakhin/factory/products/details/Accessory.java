package ru.nsu.inechakhin.factory.products.details;

import ru.nsu.inechakhin.factory.products.Product;

public class Accessory extends Product {
    public Accessory (String ID) {
        super(ID);
    }
}
