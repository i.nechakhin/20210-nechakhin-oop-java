package ru.nsu.inechakhin.factory.storages;

import ru.nsu.inechakhin.factory.products.Product;

import java.util.LinkedList;

import org.apache.log4j.Logger;

public class Storage <T extends Product> implements Storagable<T> {
    private static final Logger logger = Logger.getLogger(Storage.class.getName());

    // content
    private final LinkedList<T> products;

    // storage parameters
    private final String storageType;
    private final int storageSize;
    private int allTimeProducts = 0;

    public Storage (String type, int size) {
        storageType = type;
        storageSize = size;
        products = new LinkedList<>();

        logger.info(storageType + " > CREATED");
    }

    @Override
    public synchronized T get () throws InterruptedException {
        while (products.isEmpty()) {
            logger.info(storageType + " > STORAGE IS EMPTY");
            wait();
        }

        T product = products.getLast();
        products.removeLast();

        logger.info(storageType + " > GET PRODUCT " + product.getID() + ", FILLED: " + products.size() + "/" + storageSize);

        notifyAll();

        logger.info(storageType + " > NOTIFYING > " + products.size() + " PRODUCTS IN STORAGE");

        return product;
    }

    @Override
    public synchronized void put (T product) throws InterruptedException {
        while (products.size() >= storageSize) {
            logger.info(storageType + " > STORAGE IS FULL");
            wait();
        }

        products.add(product);
        allTimeProducts++;

        logger.info(storageType + " > PUT PRODUCT " + product.getID() + ", FILLED: " + products.size() + "/" + storageSize);

        notifyAll();

        logger.info(storageType + " > NOTIFYING > " + products.size() + " PRODUCTS IN STORAGE");
    }

    @Override
    public int getProductNum () {
        return products.size();
    }

    @Override
    public int getAllTimeProductNum () {
        return allTimeProducts;
    }
}