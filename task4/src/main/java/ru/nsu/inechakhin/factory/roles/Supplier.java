package ru.nsu.inechakhin.factory.roles;

import org.apache.log4j.Logger;
import ru.nsu.inechakhin.factory.products.Product;
import ru.nsu.inechakhin.factory.storages.Storage;

public class Supplier <T extends Product> implements Runnable {
    private static final Logger logger = Logger.getLogger(Supplier.class.getName());

    private final Storage<T> storage;

    // detail parameters
    private final Class<T> detailClass;
    private final String detailType;

    // supplier parameters
    private final String supplierType;
    private final int supplierID;
    private int suppliesCount = 0;
    private static int sleepTimeMs;

    public Supplier (int ID, Storage<T> storage, String supplierType, String detailType, Class<T> detailClass, int startSleepTimeMs) {
        supplierID = ID;
        this.storage = storage;
        this.supplierType = supplierType;
        this.detailType = detailType;
        this.detailClass = detailClass;
        sleepTimeMs = startSleepTimeMs;

        logger.info(supplierType + " " + supplierID + " > CREATED");
    }

    @Override
    public void run() {
        logger.info(supplierType + " " + supplierID + " > START WORKING");

        while (true) {
            try {
                logger.info(supplierType + " " + supplierID + " > PREPARING DETAIL FOR SUPPLY");
                suppliesCount++;
                String supplyID = generateSupplyID();
                T detail = detailClass.getDeclaredConstructor(String.class).newInstance(detailType + "_" + supplyID);

                logger.info(supplierType + " " + supplierID + " > SENDING DETAIL TO STORAGE");
                storage.put(detail);
                logger.info(supplierType + " " + supplierID + " > DETAIL SENDED");

                Thread.sleep(sleepTimeMs);
            } catch (Exception e) {
                logger.error(e.getMessage());
                e.printStackTrace();

                // finishing
                logger.info(supplierType + " " + supplierID + " > FINISHED WORKING");
                Thread.currentThread().interrupt();
            }
        }
    }

    private String generateSupplyID() {
        return supplierID + "_" + suppliesCount;
    }

    public static void setTime (int time) {
        sleepTimeMs = time;
    }
}
