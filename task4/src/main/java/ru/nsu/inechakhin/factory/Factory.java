package ru.nsu.inechakhin.factory;

import org.apache.log4j.Logger;
import ru.nsu.inechakhin.factory.products.Car;
import ru.nsu.inechakhin.factory.products.details.*;
import ru.nsu.inechakhin.factory.roles.*;
import ru.nsu.inechakhin.factory.storages.*;
import ru.nsu.inechakhin.threadpool.*;

import java.io.IOException;
import java.util.Properties;

public class Factory {
    private static final Logger logger = Logger.getLogger(Factory.class.getName());

    private int engineStorageSize;
    private int bodyStorageSize;
    private int carStorageSize;
    private int accessoryStorageSize;
    private int countFactoryWorkers;
    private int countCarDealers;
    private int countBodySuppliers;
    private int countEngineSuppliers;
    private int countAccessorySuppliers;
    private int suppliesSleepTimeMs;
    private int dealersSleepTimeMs;

    private ThreadPool threadPool;
    private Storage<Body> bodyStorage;
    private Storage<Engine> engineStorage;
    private Storage<Accessory> accessoryStorage;
    private ControlledStorage<Car> carStorage;

    private static volatile Factory instance;

    private Factory () {}

    public static Factory getInstance () {
        logger.info("FACTORY > GET INSTANCE");
        synchronized (Factory.class) {
            if (instance == null) {
                instance = new Factory();
            }
        }
        return instance;
    }

    public void getWorkPlan (String config) {
        logger.info("FACTORY > GET WORK PLAN > START");

        Properties properties = new Properties();
        try {
            properties.load(this.getClass().getResourceAsStream(config));
        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        this.engineStorageSize = Integer.parseInt(properties.getProperty("ENGINES_STORAGE_SIZE"));
        this.bodyStorageSize = Integer.parseInt(properties.getProperty("BODIES_STORAGE_SIZE"));
        this.accessoryStorageSize = Integer.parseInt(properties.getProperty("ACCESSORIES_STORAGE_SIZE"));
        this.carStorageSize = Integer.parseInt(properties.getProperty("CARS_STORAGE_SIZE"));
        this.countFactoryWorkers = Integer.parseInt(properties.getProperty("COUNT_WORKERS"));
        this.countAccessorySuppliers = Integer.parseInt(properties.getProperty("COUNT_ACCESSORIES_SUPPLIERS"));
        this.countBodySuppliers = Integer.parseInt(properties.getProperty("COUNT_BODY_SUPPLIERS"));
        this.countEngineSuppliers = Integer.parseInt(properties.getProperty("COUNT_ENGINE_SUPPLIERS"));
        this.countCarDealers = Integer.parseInt(properties.getProperty("COUNT_DEALERS"));
        this.suppliesSleepTimeMs = Integer.parseInt(properties.getProperty("SUPPLIERS_SLEEP_TIME_MS"));
        this.dealersSleepTimeMs = Integer.parseInt(properties.getProperty("DEALERS_SLEEP_TIME_MS"));

        logger.info("FACTORY > GET WORK PLAN > END");
    }

    public void startWork () {
        logger.info("FACTORY > START WORK");

        bodyStorage = new Storage<>(Body.class.getName(), bodyStorageSize);
        engineStorage = new Storage<>(Engine.class.getName(), engineStorageSize);
        accessoryStorage = new Storage<>(Accessory.class.getName(), accessoryStorageSize);
        carStorage = new ControlledStorage<>(Car.class.getName(), carStorageSize);

        Worker[] workers = new Worker[countFactoryWorkers];
        for (int i = 0; i < countFactoryWorkers; i++) {
            workers[i] = new Worker(i, accessoryStorage, bodyStorage, engineStorage, carStorage);
        }
        logger.info("FACTORY > CREATE WORKERS");

        for (int i = 0; i < countAccessorySuppliers; i++) {
            Thread accessorySupplier = new Thread(
                    new Supplier<>(i, accessoryStorage, "ACCESSORY_SUPPLIER", Accessory.class.getName(), Accessory.class, suppliesSleepTimeMs),
                    "AccessorySupplier#" + i
            );
            accessorySupplier.start();
        }
        logger.info("FACTORY > CREATE ACCESSORY SUPPLIERS");

        for (int i = 0; i < countBodySuppliers; i++) {
            Thread bodySupplier = new Thread(
                    new Supplier<>(i, bodyStorage, "BODY_SUPPLIER", Body.class.getName(), Body.class, suppliesSleepTimeMs),
                    "BodySupplier#" + i
            );
            bodySupplier.start();
        }
        logger.info("FACTORY > CREATE BODY SUPPLIERS");

        for (int i = 0; i < countEngineSuppliers; i++) {
            Thread engineSupplier = new Thread(
                    new Supplier<>(i, engineStorage, "ENGINE_STORAGE", Engine.class.getName(), Engine.class, suppliesSleepTimeMs),
                    "EngineSupplier#" + i
            );
            engineSupplier.start();
        }
        logger.info("FACTORY > CREATE ENGINE SUPPLIERS");

        for (int i = 0; i < countCarDealers; i++) {
            Thread dealer = new Thread(new Dealer(i, carStorage, dealersSleepTimeMs), "Dealer#" + i);
            dealer.start();
        }
        logger.info("FACTORY > CREATE DEALERS");

        threadPool = new ThreadPool(countFactoryWorkers, workers);
        logger.info("FACTORY > CREATE THREAD POOL");

        Controller controller = new Controller(carStorage, threadPool);
        carStorage.addController(controller);
        logger.info("FACTORY > ADD CONTROLLER TO CAR STORAGE");
        new Thread(controller, "Controller").start();
    }

    public void setDealersTime (int time) {
        Dealer.setTime(time * 1000);
    }

    public void setAccessoryProviderTime (int time) {
        Supplier.setTime(time * 1000);
    }

    public void setBodyProviderTime (int time) {
        Supplier.setTime(time * 1000);
    }

    public void setEngineProviderTime (int time) {
        Supplier.setTime(time * 1000);
    }

    public int getBodyStorageProductNum () {
        return this.bodyStorage.getProductNum();
    }

    public int getAccessoryStorageProductNum () {
        return this.accessoryStorage.getProductNum();
    }

    public int getEngineStorageProductNum () {
        return this.engineStorage.getProductNum();
    }

    public int getCarStorageProductNum () {
        return this.carStorage.getProductNum();
    }

    public int getEngineStorageSize () {
        return engineStorageSize;
    }

    public int getBodyStorageSize () {
        return bodyStorageSize;
    }

    public int getCarStorageSize () {
        return carStorageSize;
    }

    public int getAccessoryStorageSize () {
        return accessoryStorageSize;
    }

    public int getBodyStorageAllTimeProductNum () {
        return this.bodyStorage.getAllTimeProductNum();
    }

    public int getEngineStorageAllTimeProductNum () {
        return this.engineStorage.getAllTimeProductNum();
    }

    public int getAccessoryStorageAllTimeProductNum () {
        return this.accessoryStorage.getAllTimeProductNum();
    }

    public int getCarStorageAllTimeProductNum () {
        return this.carStorage.getAllTimeProductNum();
    }

    public int getTasksNum () {
        return this.threadPool.getTasksNum();
    }
}
