package ru.nsu.inechakhin.factory.products.details;

import ru.nsu.inechakhin.factory.products.Product;

public class Engine extends Product {
    public Engine (String ID) {
        super(ID);
    }
}
