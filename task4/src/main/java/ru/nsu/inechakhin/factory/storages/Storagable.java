package ru.nsu.inechakhin.factory.storages;

import ru.nsu.inechakhin.factory.products.Product;

public interface Storagable <T extends Product> {
    T get () throws InterruptedException;

    void put (T product) throws InterruptedException;

    int getProductNum ();

    int getAllTimeProductNum ();
}
