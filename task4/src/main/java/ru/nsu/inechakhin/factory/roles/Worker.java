package ru.nsu.inechakhin.factory.roles;

import org.apache.log4j.Logger;
import ru.nsu.inechakhin.factory.products.Car;
import ru.nsu.inechakhin.factory.products.details.*;
import ru.nsu.inechakhin.factory.storages.*;
import ru.nsu.inechakhin.threadpool.*;

import java.util.List;

public class Worker extends Executor {
    private List<ThreadPoolTask> taskQueue;

    private static final Logger logger = Logger.getLogger(Worker.class.getName());

    // storages
    private final Storage<Accessory> accessoryStorage;
    private final Storage<Body> bodyStorage;
    private final Storage<Engine> engineStorage;
    private final ControlledStorage<Car> carStorage;

    // worker parameters
    private final int workerID;
    private int madeCarsCount = 0;

    public Worker (int ID, Storage<Accessory> accessoryStorage, Storage<Body> bodyStorage, Storage<Engine> engineStorage, ControlledStorage<Car> carStorage) {
        workerID = ID;
        this.accessoryStorage = accessoryStorage;
        this.bodyStorage = bodyStorage;
        this.engineStorage = engineStorage;
        this.carStorage = carStorage;

        logger.info("WORKER " + workerID + " > CREATED");
    }

    public void addTaskQueue (List<ThreadPoolTask> list) {
        this.taskQueue = list;
    }

    public void performTask (ThreadPoolTask t) {
        t.start();
        try {
            logger.info("WORKER " + workerID + " > GETTING DETAILS FROM STORAGES");

            Accessory accessory = accessoryStorage.get();
            Body body = bodyStorage.get();
            Engine engine = engineStorage.get();

            logger.info("WORKER " + workerID + " > GOT ALL DETAILS FROM STORAGES");

            Car car = createCar(body, engine, accessory);
            carStorage.put(car);

            logger.info("WORKER " + workerID + " > PUTTED CREATED CAR IN STORAGE");
        } catch (InterruptedException ex) {
            t.interrupted();
        }
        t.finish();
    }

    private Car createCar (Body body, Engine engine, Accessory accessory) {
        madeCarsCount++;
        String carID = generateNewCarID();

        logger.info("WORKER " + workerID + " > CREATING CAR " + carID);

        return new Car(body, engine, accessory, carID);
    }

    private String generateNewCarID () {
        return "CAR_" + workerID + "_" + madeCarsCount;
    }

    @Override
    public void run () {
        logger.info("WORKER " + workerID + " > THREAD " + Thread.currentThread().getName() + " > STARTED");

        ThreadPoolTask toExecute;
        while (true) {
            synchronized (taskQueue) {
                if (taskQueue.isEmpty()) {
                    try {
                        taskQueue.wait();
                    } catch (InterruptedException ex) {
                        logger.error("WORKER " + workerID + " > THREAD " + Thread.currentThread().getName() + " > INTERRUPTED");
                    }
                    continue;
                } else {
                    toExecute = taskQueue.remove(0);
                }
            }
            logger.info("WORKER " + workerID + " > THREAD " + Thread.currentThread().getName() + " > GOT JOB " + toExecute.getName());
            performTask(toExecute);
        }
    }
}