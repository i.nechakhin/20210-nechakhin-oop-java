package ru.nsu.inechakhin.factory.storages;

import org.apache.log4j.Logger;
import ru.nsu.inechakhin.factory.products.Product;
import ru.nsu.inechakhin.factory.roles.Controller;

import java.util.LinkedList;

public class ControlledStorage <T extends Product> implements Storagable<T> {
    private static final Logger logger = Logger.getLogger(Storage.class.getName());

    Controller controller;

    // content
    private final LinkedList<T> products;

    // storage parameters
    private final String storageType;
    private final int storageSize;
    private int allTimeProducts = 0;
    private int countOrders = 0;

    public ControlledStorage (String type, int size) {
        storageType = type;
        storageSize = size;
        products = new LinkedList<>();

        logger.info(storageType + " > CREATED");
    }

    @Override
    public synchronized T get () throws InterruptedException {
        while (products.isEmpty()) {
            logger.info(storageType + " > STORAGE IS EMPTY");
            wait();
        }

        T product = products.getLast();
        products.removeLast();
        countOrders--;

        logger.info(storageType + " > GET PRODUCT " + product.getID() + ", FILLED: " + products.size() + "/" + storageSize);

        controller.analyze();
        logger.info(storageType + " > ANALYZE");

        notifyAll();

        logger.info(storageType + " > NOTIFYING > " + products.size() + " PRODUCTS IN STORAGE");

        return product;
    }

    @Override
    public synchronized void put (T product) throws InterruptedException {
        while (products.size() >= storageSize) {
            logger.info(storageType + " > STORAGE IS FULL");
            wait();
        }

        products.add(product);
        allTimeProducts++;

        logger.info(storageType + " > PUT PRODUCT " + product.getID() + ", FILLED: " + products.size() + "/" + storageSize);

        notifyAll();

        logger.info(storageType + " > NOTIFYING > " + products.size() + " PRODUCTS IN STORAGE");
    }

    public synchronized void addOrder () {
        countOrders++;
    }

    public synchronized int getCountOrders () {
        return Math.max(countOrders - products.size(), 0);
    }

    public void addController (Controller controller) {
        this.controller = controller;
    }

    public synchronized boolean orderCondition () {
        if (countOrders > products.size()) {
            return true;
        } else {
            return countOrders == 0 && products.size() == 0;
        }
    }

    @Override
    public int getProductNum () {
        return products.size();
    }

    @Override
    public int getAllTimeProductNum () {
        return allTimeProducts;
    }
}
