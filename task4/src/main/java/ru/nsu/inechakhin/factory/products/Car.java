package ru.nsu.inechakhin.factory.products;

import ru.nsu.inechakhin.factory.products.details.*;

public class Car extends Product {
    private final Body body;
    private final Engine engine;
    private final Accessory accessory;

    public Car (Body body, Engine engine, Accessory accessory, String ID) {
        super(ID);  // setting product ID
        this.body = body;
        this.engine = engine;
        this.accessory = accessory;
    }

    public String getBodyID () {
        return body.getID();
    }

    public String getEngineID () {
        return engine.getID();
    }

    public String getAccessoryID () {
        return accessory.getID();
    }
}
