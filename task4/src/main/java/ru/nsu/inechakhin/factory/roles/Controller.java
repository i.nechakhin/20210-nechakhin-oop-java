package ru.nsu.inechakhin.factory.roles;

import org.apache.log4j.Logger;
import ru.nsu.inechakhin.factory.products.Car;
import ru.nsu.inechakhin.factory.storages.ControlledStorage;
import ru.nsu.inechakhin.threadpool.Task;
import ru.nsu.inechakhin.threadpool.ThreadPool;

public class Controller implements Runnable {
    private static final Logger logger = Logger.getLogger(Controller.class.getName());

    private final ControlledStorage<Car> carStorage;
    private final ThreadPool threadPool;
    private boolean isSuspended = false;

    public Controller (ControlledStorage<Car> carStorage, ThreadPool threadPool) {
        this.carStorage = carStorage;
        this.threadPool = threadPool;

        logger.info("CONTROLLER > CREATED");
    }

    @Override
    public void run () {
        logger.info("CONTROLLER > START WORKING");

        while (true) {
            synchronized (this) {
                while (isSuspended) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        logger.info("CONTROLLER > FINISHED WORKING");
                        System.out.println(e.getMessage());
                    }
                }
            }
            if (carStorage.orderCondition()) {
                logger.info("CONTROLLER > CREATING WORKERS");
                for (int i = 0; i < (carStorage.getCountOrders() > threadPool.getTasksNum() ? (carStorage.getCountOrders() - threadPool.getTasksNum()) : 0) + 1; ++i) {
                    threadPool.addTask(new Task() {
                        @Override
                        public String getName () {
                            return "TASK!";
                        }

                        @Override
                        public void performWork () {
                            System.out.println("Work done!");
                        }
                    });
                }
            }
            this.suspend();
        }
    }

    public void analyze () {
        this.resume();
    }

    private void suspend () {
        this.isSuspended = true;
        logger.info("CONTROLLER > SUSPEND");
    }

    private void resume () {
        this.isSuspended = false;
        synchronized (this) {
            this.notify();
        }
        logger.info("CONTROLLER > RESUME");
    }

}
