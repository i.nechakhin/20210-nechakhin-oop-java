package ru.nsu.inechakhin.view;

import ru.nsu.inechakhin.factory.Factory;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;

public class View extends JFrame {

    public static void start (Factory factory) {
        EventQueue.invokeLater(() -> {
            View frame = new View(factory);
            frame.setTitle("Factory Emulator");
            frame.setSize(500, 500);
            frame.setResizable(false);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
            frame.repaint();
        });
    }

    private final JPanel sliderPanel;

    @Override
    public void paint (Graphics g) {
        sliderPanel.repaint();
    }

    public View (Factory factory) {
        sliderPanel = new JPanel() {
            final Thread thread = new Thread(() -> {
                while (true) {
                    repaint();
                }
            });

            {
                thread.setDaemon(true);
                thread.start();
            }

            @Override
            public void paint (Graphics g) {
                super.paint(g);
                Graphics2D g2d = (Graphics2D) g;
                g2d.drawString("Body storage: " + factory.getBodyStorageProductNum() + " / " + factory.getBodyStorageSize()
                        + " All time: " + factory.getBodyStorageAllTimeProductNum(), 15, 250);
                g2d.drawString("Engine storage: " + factory.getEngineStorageProductNum() + " / " + factory.getEngineStorageSize()
                        + " All time: " + factory.getEngineStorageAllTimeProductNum(), 15, 270);
                g2d.drawString("Accessory storage: " + factory.getAccessoryStorageProductNum() + " / " + factory.getAccessoryStorageSize()
                        + " All time: " + factory.getAccessoryStorageAllTimeProductNum(), 15, 290);
                g2d.drawString("Car storage: " + factory.getCarStorageProductNum() + " / " + factory.getCarStorageSize()
                        + " All time: " + factory.getCarStorageAllTimeProductNum(), 15, 310);
                g2d.drawString("Tasks number: " + factory.getTasksNum(), 15, 330);
            }
        };

        sliderPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

        JSlider dealers = new JSlider(JSlider.HORIZONTAL, 1, 11, 10);
        dealers.setMajorTickSpacing(5);
        dealers.setMinorTickSpacing(1);
        dealers.setPaintTicks(true);
        dealers.setSnapToTicks(true);
        addSlider(dealers, " Dealers time", event -> {
            JSlider source = (JSlider) event.getSource();
            factory.setDealersTime(source.getValue());
        });

        JSlider engineSupplier = new JSlider(JSlider.HORIZONTAL, 1, 11, 3);
        engineSupplier.setMajorTickSpacing(5);
        engineSupplier.setMinorTickSpacing(1);
        engineSupplier.setPaintTicks(true);
        engineSupplier.setSnapToTicks(true);
        addSlider(engineSupplier, " Engine Providers time", event -> {
            JSlider source = (JSlider) event.getSource();
            factory.setEngineProviderTime(source.getValue());
        });

        JSlider bodySupplier = new JSlider(JSlider.HORIZONTAL, 1, 11, 2);
        bodySupplier.setMajorTickSpacing(5);
        bodySupplier.setMinorTickSpacing(1);
        bodySupplier.setPaintTicks(true);
        bodySupplier.setSnapToTicks(true);
        addSlider(bodySupplier, " Body Providers time", event -> {
            JSlider source = (JSlider) event.getSource();
            factory.setBodyProviderTime(source.getValue());
        });

        JSlider accessorySupplier = new JSlider(JSlider.HORIZONTAL, 1, 11, 5);
        accessorySupplier.setMajorTickSpacing(5);
        accessorySupplier.setMinorTickSpacing(1);
        accessorySupplier.setPaintTicks(true);
        accessorySupplier.setSnapToTicks(true);
        addSlider(accessorySupplier, " Accessory Providers time", event -> {
            JSlider source = (JSlider) event.getSource();
            factory.setAccessoryProviderTime(source.getValue());
        });

        add(sliderPanel, BorderLayout.CENTER);
    }

    public void addSlider (JSlider s, String description, ChangeListener listen) {
        s.addChangeListener(listen);
        JPanel panel = new JPanel();
        panel.add(s);
        panel.add(new JLabel(description));
        sliderPanel.add(panel);
    }
}
