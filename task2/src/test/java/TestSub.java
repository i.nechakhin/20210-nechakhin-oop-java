import org.junit.Assert;
import org.junit.Test;
import ru.ccfit.inechakhin.stackcalc.commandfactory.commands.Sub;
import ru.ccfit.inechakhin.stackcalc.exceptions.NotEnoughElemException;
import ru.ccfit.inechakhin.stackcalc.exceptions.NullStackException;

import java.util.HashMap;
import java.util.Stack;

public class TestSub {
    @Test
    public void testThrowNullStackException () {
        Sub sub = new Sub();
        HashMap<String, Double> map = null;
        Stack<Double> stack = null;
        String param = "";
        Throwable throwable = Assert.assertThrows(NullStackException.class, () -> sub.execute(map, stack, param));
        Assert.assertNotNull(throwable.getMessage());
    }

    @Test
    public void testThrowNotEnoughElemException () {
        Sub sub = new Sub();
        HashMap<String, Double> map = null;
        Stack<Double> stack = new Stack<>();
        stack.push(4.0);
        String param = "";
        Throwable throwable = Assert.assertThrows(NotEnoughElemException.class, () -> sub.execute(map, stack, param));
        Assert.assertNotNull(throwable.getMessage());
    }

    @Test
    public void testSub () {
        Sub sub = new Sub();
        HashMap<String, Double> map = null;
        Stack<Double> stack = new Stack<>();
        stack.push(4.0);
        stack.push(3.0);
        String param = "";
        sub.execute(map, stack, param);
        Assert.assertEquals((Double) 1.0, stack.lastElement());
    }
}
