import org.junit.Assert;
import org.junit.Test;
import ru.ccfit.inechakhin.stackcalc.commandfactory.commands.Pop;
import ru.ccfit.inechakhin.stackcalc.exceptions.NullStackException;

import java.util.HashMap;
import java.util.Stack;

public class TestPop {
    @Test
    public void testThrowNullStackException () {
        Pop pop = new Pop();
        HashMap<String, Double> map = null;
        Stack<Double> stack = null;
        String param = "";
        Throwable throwable = Assert.assertThrows(NullStackException.class, () -> pop.execute(map, stack, param));
        Assert.assertNotNull(throwable.getMessage());
    }

    @Test
    public void testPop () {
        Pop pop = new Pop();
        HashMap<String, Double> map = null;
        Stack<Double> stack = new Stack<>();
        stack.push(4.0);
        stack.push(2.0);
        String param = "";
        Assert.assertEquals((Double) 2.0, stack.lastElement());
        pop.execute(map, stack, param);
        Assert.assertEquals((Double) 4.0, stack.lastElement());
    }
}
