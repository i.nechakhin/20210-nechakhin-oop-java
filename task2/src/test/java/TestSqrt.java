import org.junit.Assert;
import org.junit.Test;
import ru.ccfit.inechakhin.stackcalc.commandfactory.commands.Sqrt;
import ru.ccfit.inechakhin.stackcalc.exceptions.NotEnoughElemException;
import ru.ccfit.inechakhin.stackcalc.exceptions.NullStackException;
import ru.ccfit.inechakhin.stackcalc.exceptions.RootOfNegException;

import java.util.HashMap;
import java.util.Stack;

public class TestSqrt {
    @Test
    public void testThrowNullStackException () {
        Sqrt sqrt = new Sqrt();
        HashMap<String, Double> map = null;
        Stack<Double> stack = null;
        String param = "";
        Throwable throwable = Assert.assertThrows(NullStackException.class, () -> sqrt.execute(map, stack, param));
        Assert.assertNotNull(throwable.getMessage());
    }

    @Test
    public void testThrowNotEnoughElemException () {
        Sqrt sqrt = new Sqrt();
        HashMap<String, Double> map = null;
        Stack<Double> stack = new Stack<>();
        String param = "";
        Throwable throwable = Assert.assertThrows(NotEnoughElemException.class, () -> sqrt.execute(map, stack, param));
        Assert.assertNotNull(throwable.getMessage());
    }

    @Test
    public void testThrowRootOfNegException () {
        Sqrt sqrt = new Sqrt();
        HashMap<String, Double> map = null;
        Stack<Double> stack = new Stack<>();
        stack.push(-4.0);
        String param = "";
        Throwable throwable = Assert.assertThrows(RootOfNegException.class, () -> sqrt.execute(map, stack, param));
        Assert.assertNotNull(throwable.getMessage());
    }

    @Test
    public void testSqrt () {
        Sqrt sqrt = new Sqrt();
        HashMap<String, Double> map = null;
        Stack<Double> stack = new Stack<>();
        stack.push(4.0);
        String param = "";
        sqrt.execute(map, stack, param);
        Assert.assertEquals((Double) 2.0, stack.lastElement());
    }
}
