import org.junit.Assert;
import org.junit.Test;
import ru.ccfit.inechakhin.stackcalc.commandfactory.commands.Add;
import ru.ccfit.inechakhin.stackcalc.exceptions.NotEnoughElemException;
import ru.ccfit.inechakhin.stackcalc.exceptions.NullStackException;

import java.util.HashMap;
import java.util.Stack;

public class TestAdd {
    @Test
    public void testThrowNullStackException () {
        Add add = new Add();
        HashMap<String, Double> map = null;
        Stack<Double> stack = null;
        String param = "";
        Throwable throwable = Assert.assertThrows(NullStackException.class, () -> add.execute(map, stack, param));
        Assert.assertNotNull(throwable.getMessage());
    }

    @Test
    public void testThrowNotEnoughElemException () {
        Add add = new Add();
        HashMap<String, Double> map = null;
        Stack<Double> stack = new Stack<>();
        stack.push(4.0);
        String param = "";
        Throwable throwable = Assert.assertThrows(NotEnoughElemException.class, () -> add.execute(map, stack, param));
        Assert.assertNotNull(throwable.getMessage());
    }

    @Test
    public void testAdd () {
        Add add = new Add();
        HashMap<String, Double> map = null;
        Stack<Double> stack = new Stack<>();
        stack.push(4.0);
        stack.push(2.0);
        String param = "";
        add.execute(map, stack, param);
        Assert.assertEquals((Double) 6.0, stack.lastElement());
    }
}
