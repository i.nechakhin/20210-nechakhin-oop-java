import org.junit.Assert;
import org.junit.Test;
import ru.ccfit.inechakhin.stackcalc.commandfactory.commands.Push;
import ru.ccfit.inechakhin.stackcalc.exceptions.NullStackException;

import java.util.HashMap;
import java.util.Stack;

public class TestPush {
    @Test
    public void testThrowNullStackException () {
        Push push = new Push();
        HashMap<String, Double> map = null;
        Stack<Double> stack = null;
        String param = "";
        Throwable throwable = Assert.assertThrows(NullStackException.class, () -> push.execute(map, stack, param));
        Assert.assertNotNull(throwable.getMessage());
    }

    @Test
    public void testPush () {
        Push push = new Push();
        HashMap<String, Double> map = new HashMap<>();
        Stack<Double> stack = new Stack<>();
        String param = "4";
        push.execute(map, stack, param);
        Assert.assertEquals((Double) 4.0, stack.lastElement());
        map.put("var", 2.0);
        param = "var";
        push.execute(map, stack, param);
        Assert.assertEquals((Double) 2.0, stack.lastElement());
    }
}
