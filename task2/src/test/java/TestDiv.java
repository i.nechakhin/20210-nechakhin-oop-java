import org.junit.Assert;
import org.junit.Test;
import ru.ccfit.inechakhin.stackcalc.commandfactory.commands.Div;
import ru.ccfit.inechakhin.stackcalc.exceptions.DivByZeroException;
import ru.ccfit.inechakhin.stackcalc.exceptions.NotEnoughElemException;
import ru.ccfit.inechakhin.stackcalc.exceptions.NullStackException;

import java.util.HashMap;
import java.util.Stack;

public class TestDiv {
    @Test
    public void testThrowNullStackException () {
        Div div = new Div();
        HashMap<String, Double> map = null;
        Stack<Double> stack = null;
        String param = "";
        Throwable throwable = Assert.assertThrows(NullStackException.class, () -> div.execute(map, stack, param));
        Assert.assertNotNull(throwable.getMessage());
    }

    @Test
    public void testThrowNotEnoughElemException () {
        Div div = new Div();
        HashMap<String, Double> map = null;
        Stack<Double> stack = new Stack<>();
        stack.push(4.0);
        String param = "";
        Throwable throwable = Assert.assertThrows(NotEnoughElemException.class, () -> div.execute(map, stack, param));
        Assert.assertNotNull(throwable.getMessage());
    }

    @Test
    public void testThrowDivByZeroException () {
        Div div = new Div();
        HashMap<String, Double> map = null;
        Stack<Double> stack = new Stack<>();
        stack.push(4.0);
        stack.push(0.0);
        String param = "";
        Throwable throwable = Assert.assertThrows(DivByZeroException.class, () -> div.execute(map, stack, param));
        Assert.assertNotNull(throwable.getMessage());
    }

    @Test
    public void testDiv () {
        Div div = new Div();
        HashMap<String, Double> map = null;
        Stack<Double> stack = new Stack<>();
        stack.push(8.0);
        stack.push(4.0);
        String param = "";
        div.execute(map, stack, param);
        Assert.assertEquals((Double) 2.0, stack.lastElement());
    }
}
