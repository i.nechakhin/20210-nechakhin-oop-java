import org.junit.Assert;
import org.junit.Test;
import ru.ccfit.inechakhin.stackcalc.commandfactory.commands.Define;
import ru.ccfit.inechakhin.stackcalc.exceptions.NullMapException;

import java.util.HashMap;
import java.util.Stack;

public class TestDefine {
    @Test
    public void testThrowNullMapException () {
        Define define = new Define();
        Stack<Double> stack = new Stack<>();
        HashMap<String, Double> map = null;
        String param = "";
        Throwable throwable = Assert.assertThrows(NullMapException.class, () -> define.execute(map, stack, param));
        Assert.assertNotNull(throwable.getMessage());
    }

    @Test
    public void testDefine () {
        Define define = new Define();
        Stack<Double> stack = new Stack<>();
        HashMap<String, Double> map = new HashMap<>();
        String param = "var1 4";
        define.execute(map, stack, param);
        Assert.assertEquals((Double) 4.0, map.get("var1"));
    }
}
