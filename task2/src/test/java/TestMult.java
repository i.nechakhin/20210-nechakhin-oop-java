import org.junit.Assert;
import org.junit.Test;
import ru.ccfit.inechakhin.stackcalc.commandfactory.commands.Mult;
import ru.ccfit.inechakhin.stackcalc.exceptions.NotEnoughElemException;
import ru.ccfit.inechakhin.stackcalc.exceptions.NullStackException;

import java.util.HashMap;
import java.util.Stack;

public class TestMult {
    @Test
    public void testThrowNullStackException () {
        Mult mult = new Mult();
        HashMap<String, Double> map = null;
        Stack<Double> stack = null;
        String param = "";
        Throwable throwable = Assert.assertThrows(NullStackException.class, () -> mult.execute(map, stack, param));
        Assert.assertNotNull(throwable.getMessage());
    }

    @Test
    public void testThrowNotEnoughElemException () {
        Mult mult = new Mult();
        HashMap<String, Double> map = null;
        Stack<Double> stack = new Stack<>();
        stack.push(4.0);
        String param = "";
        Throwable throwable = Assert.assertThrows(NotEnoughElemException.class, () -> mult.execute(map, stack, param));
        Assert.assertNotNull(throwable.getMessage());
    }

    @Test
    public void testMult () {
        Mult mult = new Mult();
        HashMap<String, Double> map = null;
        Stack<Double> stack = new Stack<>();
        stack.push(4.0);
        stack.push(2.0);
        String param = "";
        mult.execute(map, stack, param);
        Assert.assertEquals((Double) 8.0, stack.lastElement());
    }

    @Test
    public void testMultOnZero () {
        Mult mult = new Mult();
        HashMap<String, Double> map = null;
        Stack<Double> stack = new Stack<>();
        stack.push(4.0);
        stack.push(0.0);
        String param = "";
        mult.execute(map, stack, param);
        Assert.assertEquals((Double) 0.0, stack.lastElement());
    }
}
