package ru.ccfit.inechakhin.stackcalc.commandfactory.commands;

import ru.ccfit.inechakhin.stackcalc.commandfactory.Command;
import ru.ccfit.inechakhin.stackcalc.exceptions.NullStackException;

import java.util.HashMap;
import java.util.Stack;
import java.util.logging.Logger;

public class Print implements Command {
    private static final Logger logger = Logger.getLogger(Print.class.getName());

    @Override
    public void execute (HashMap<String, Double> map, Stack<Double> stack, String param) {
        logger.info("Print : execute : begin");

        if (stack == null) {
            logger.info("Print : execute : error : stack is not initialized");
            throw new NullStackException("Print: stack is not initialized");
        }

        System.out.println(stack.lastElement());

        logger.info("Print : execute : end");
    }
}
