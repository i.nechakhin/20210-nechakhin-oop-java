package ru.ccfit.inechakhin.stackcalc.exceptions;

public class NoCommandException extends StackCalcException {
    public NoCommandException (String message) {
        super(message);
    }
}
