package ru.ccfit.inechakhin.stackcalc.exceptions;

public class RootOfNegException extends StackCalcException {
    public RootOfNegException (String message) {
        super(message);
    }
}
