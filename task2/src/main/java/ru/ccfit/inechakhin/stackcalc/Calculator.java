package ru.ccfit.inechakhin.stackcalc;

import ru.ccfit.inechakhin.stackcalc.commandfactory.Command;
import ru.ccfit.inechakhin.stackcalc.commandfactory.CommandFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;
import java.util.logging.Logger;

public class Calculator {
    private final HashMap<String, Double> map;
    private final Stack<Double> stack;
    private static final Logger logger = Logger.getLogger(Calculator.class.getName());

    public Calculator () {
        logger.info("Calculator : Calculator : begin");
        map = new HashMap<>();
        stack = new Stack<>();
        logger.info("Calculator : Calculator : end");
    }

    private void executeCommand (String commandLine) throws Exception {
        logger.info("Calculator : executeCommand : begin : commandLine = " + commandLine);

        StringBuilder commandName = new StringBuilder();
        StringBuilder commandParam = new StringBuilder();
        int index = 0;
        while ((index < commandLine.length()) && !Character.isWhitespace(commandLine.charAt(index))) {
            commandName.append(commandLine.charAt(index));
            index ++;
        }
        if (commandLine.length() > 1) {
            for (int i = index + 1; i < commandLine.length(); ++ i) {
                commandParam.append(commandLine.charAt(i));
            }
        }

        logger.info("Calculator : executeCommand : commandName = " + commandName + ", commandParam = " + commandParam);

        logger.info("Calculator : executeCommand : creating " + commandName);
        Command command = CommandFactory.getInstance().createCommand(commandName.toString());
        logger.info("Calculator : executeCommand : was created " + commandName);

        command.execute(map, stack, commandParam.toString());

        logger.info("Calculator : executeCommand : end");
    }

    public void calculate (String commandsFileName) {
        logger.info("Calculator : calculate(FileName) : begin");

        try {
            BufferedReader commands = new BufferedReader(new FileReader(commandsFileName));
            String commandLine;
            while ((commandLine = commands.readLine()) != null) {
                executeCommand(commandLine);
            }
        } catch (Exception e) {
            logger.info("Calculator : calculate(FileName) : error : " + e.getMessage());
            e.printStackTrace();
        }

        logger.info("Calculator : calculate(FileName) : end");
    }

    public void calculate () {
        logger.info("Calculator : calculate() : begin");

        try {
            Scanner in = new Scanner(System.in);
            System.out.println("Enter EXIT to exit");
            while (true) {
                String commandLine = in.nextLine();
                if (commandLine.equalsIgnoreCase("exit")) {
                    break;
                } else {
                    try {
                        executeCommand(commandLine);
                    } catch (Exception e) {
                        logger.info("Calculator : calculate() : error : " + e.getMessage());
                    }
                }
            }
        } catch (Exception e) {
            logger.info("Calculator : calculate() : error : " + e.getMessage());
            e.printStackTrace();
        }

        logger.info("Calculator : calculate() : end");
    }
}
