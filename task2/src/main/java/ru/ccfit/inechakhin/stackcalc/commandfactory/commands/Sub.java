package ru.ccfit.inechakhin.stackcalc.commandfactory.commands;

import ru.ccfit.inechakhin.stackcalc.commandfactory.Command;
import ru.ccfit.inechakhin.stackcalc.exceptions.NotEnoughElemException;
import ru.ccfit.inechakhin.stackcalc.exceptions.NullStackException;

import java.util.HashMap;
import java.util.Stack;
import java.util.logging.Logger;

public class Sub implements Command {
    private static final Logger logger = Logger.getLogger(Sub.class.getName());

    @Override
    public void execute (HashMap<String, Double> map, Stack<Double> stack, String param) {
        logger.info("Sub : execute : begin");

        if (stack == null) {
            logger.info("Sub : execute : error : stack is not initialized");
            throw new NullStackException("Sub: stack is not initialized");
        }
        if (stack.size() < 2) {
            logger.info("Sub : execute : error : stack has less than 2 elements");
            throw new NotEnoughElemException("Sub: stack has less than 2 elements");
        }

        Double num1 = stack.lastElement();
        stack.pop();
        Double num2 = stack.lastElement();
        stack.pop();

        logger.info("Sub : execute : num1 = " + num1.toString() + ", num2 = " + num2.toString());

        stack.push(num2 - num1);

        logger.info("Sub : execute : end");
    }
}
