package ru.ccfit.inechakhin.stackcalc.commandfactory.commands;

import ru.ccfit.inechakhin.stackcalc.commandfactory.Command;
import ru.ccfit.inechakhin.stackcalc.exceptions.DivByZeroException;
import ru.ccfit.inechakhin.stackcalc.exceptions.NotEnoughElemException;
import ru.ccfit.inechakhin.stackcalc.exceptions.NullStackException;

import java.util.HashMap;
import java.util.Stack;
import java.util.logging.Logger;

public class Div implements Command {
    private static final Logger logger = Logger.getLogger(Div.class.getName());

    @Override
    public void execute (HashMap<String, Double> map, Stack<Double> stack, String param) {
        logger.info("Div : execute : begin");

        if (stack == null) {
            logger.info("Div : execute : error : stack is not initialized");
            throw new NullStackException("Div: stack is not initialized");
        }
        if (stack.size() < 2) {
            logger.info("Div : execute : error : stack has less than 2 elements");
            throw new NotEnoughElemException("Div: stack has less than 2 elements");
        }

        Double num1 = stack.lastElement();
        stack.pop();
        Double num2 = stack.lastElement();
        stack.pop();

        logger.info("Div : execute : num1 = " + num1.toString() + ", num2 = " + num2.toString());

        if (num1 == 0) {
            logger.info("Div : execute : error : divide by zero");
            throw new DivByZeroException("Div: divide by zero");
        }

        stack.push(num2 / num1);

        logger.info("Div : execute : end");
    }
}
