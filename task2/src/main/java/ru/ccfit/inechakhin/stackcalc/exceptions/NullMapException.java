package ru.ccfit.inechakhin.stackcalc.exceptions;

public class NullMapException extends StackCalcException {
    public NullMapException (String message) {
        super(message);
    }
}
