package ru.ccfit.inechakhin.stackcalc.commandfactory.commands;

import ru.ccfit.inechakhin.stackcalc.commandfactory.Command;
import ru.ccfit.inechakhin.stackcalc.exceptions.NotEnoughElemException;
import ru.ccfit.inechakhin.stackcalc.exceptions.NullStackException;

import java.util.HashMap;
import java.util.Stack;
import java.util.logging.Logger;

public class Mult implements Command {
    private static final Logger logger = Logger.getLogger(Mult.class.getName());

    @Override
    public void execute (HashMap<String, Double> map, Stack<Double> stack, String param) {
        logger.info("Mult : execute : begin");

        if (stack == null) {
            logger.info("Mult : execute : error : stack is not initialized");
            throw new NullStackException("Mult: stack is not initialized");
        }
        if (stack.size() < 2) {
            logger.info("Mult : execute : error : stack has less than 2 elements");
            throw new NotEnoughElemException("Mult: stack has less than 2 elements");
        }

        Double num1 = stack.lastElement();
        stack.pop();
        Double num2 = stack.lastElement();
        stack.pop();

        logger.info("Mult : execute : num1 = " + num1.toString() + ", num2 = " + num2.toString());

        stack.push(num1 * num2);

        logger.info("Mult : execute : end");
    }
}
