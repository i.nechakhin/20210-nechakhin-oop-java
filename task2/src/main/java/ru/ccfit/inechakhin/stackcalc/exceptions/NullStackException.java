package ru.ccfit.inechakhin.stackcalc.exceptions;

public class NullStackException extends StackCalcException {
    public NullStackException (String message) {
        super(message);
    }
}
