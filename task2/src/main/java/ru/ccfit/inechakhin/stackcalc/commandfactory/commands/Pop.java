package ru.ccfit.inechakhin.stackcalc.commandfactory.commands;

import ru.ccfit.inechakhin.stackcalc.commandfactory.Command;
import ru.ccfit.inechakhin.stackcalc.exceptions.NullStackException;

import java.util.HashMap;
import java.util.Stack;
import java.util.logging.Logger;

public class Pop implements Command {
    private static final Logger logger = Logger.getLogger(Pop.class.getName());

    @Override
    public void execute (HashMap<String, Double> map, Stack<Double> stack, String param) {
        logger.info("Pop : execute : begin");

        if (stack == null) {
            logger.info("Pop : execute : error : stack is not initialized");
            throw new NullStackException("Pop: stack is not initialized");
        }

        stack.pop();

        logger.info("Pop : execute : end");
    }
}
