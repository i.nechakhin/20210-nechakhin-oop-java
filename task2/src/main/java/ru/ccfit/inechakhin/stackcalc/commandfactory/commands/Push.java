package ru.ccfit.inechakhin.stackcalc.commandfactory.commands;

import ru.ccfit.inechakhin.stackcalc.commandfactory.Command;
import ru.ccfit.inechakhin.stackcalc.exceptions.NullStackException;

import java.util.HashMap;
import java.util.Stack;
import java.util.logging.Logger;

public class Push implements Command {
    private static final Logger logger = Logger.getLogger(Push.class.getName());

    @Override
    public void execute (HashMap<String, Double> map, Stack<Double> stack, String param) {
        logger.info("Push : execute : begin");

        if (stack == null) {
            logger.info("Push : execute : error : stack is not initialized");
            throw new NullStackException("Push: stack is not initialized");
        }

        logger.info("Push : execute : param = " + param);

        if (param.matches("[0-9]+")) {
            stack.push(Double.parseDouble(param));
        } else {
            stack.push(map.get(param));
        }

        logger.info("Push : execute : end");
    }
}
