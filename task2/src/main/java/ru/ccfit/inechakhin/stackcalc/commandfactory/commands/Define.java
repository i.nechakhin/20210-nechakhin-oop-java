package ru.ccfit.inechakhin.stackcalc.commandfactory.commands;

import ru.ccfit.inechakhin.stackcalc.commandfactory.Command;
import ru.ccfit.inechakhin.stackcalc.exceptions.NullMapException;

import java.util.HashMap;
import java.util.Stack;
import java.util.logging.Logger;

public class Define implements Command {
    private static final Logger logger = Logger.getLogger(Define.class.getName());

    @Override
    public void execute (HashMap<String, Double> map, Stack<Double> stack, String param) {
        logger.info("Define : execute : begin");

        if (map == null) {
            logger.info("Define : execute : error : map is not initialized");
            throw new NullMapException("Define: map is not initialized");
        }

        StringBuilder varName = new StringBuilder();
        StringBuilder varNumStr = new StringBuilder();
        int index = 0;
        while (!Character.isWhitespace(param.charAt(index))) {
            varName.append(param.charAt(index));
            index ++;
        }
        for (int i = index + 1; i < param.length(); ++ i) {
            varNumStr.append(param.charAt(i));
        }

        logger.info("Define : execute : varName = " + varName + ", varNum = " + varNumStr);

        Double varNum = Double.parseDouble(varNumStr.toString());
        map.put(varName.toString(), varNum);

        logger.info("Define : execute : end");
    }
}
