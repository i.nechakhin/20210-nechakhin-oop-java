package ru.ccfit.inechakhin.stackcalc.commandfactory;

import java.util.HashMap;
import java.util.Stack;

public interface Command {
    void execute (HashMap<String, Double> map, Stack<Double> stack, String param);
}
