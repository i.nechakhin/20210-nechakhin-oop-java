package ru.ccfit.inechakhin.stackcalc.exceptions;

public class StackCalcException extends RuntimeException {
    public StackCalcException (String message) {
        super("StackCalcException: " + message);
    }
}
