package ru.ccfit.inechakhin.stackcalc.commandfactory;

import ru.ccfit.inechakhin.stackcalc.exceptions.NoCommandException;

import java.io.*;
import java.lang.reflect.Constructor;
import java.util.Properties;
import java.util.logging.Logger;

public class CommandFactory {
    private static CommandFactory factory = null;
    private final Properties properties;
    private static final Logger logger = Logger.getLogger(CommandFactory.class.getName());

    private CommandFactory() throws IOException {
        logger.info("CommandFactory : CommandFactory : begin");

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("conf.properties");

        if (inputStream == null) {
            logger.info("CommandFactory : CommandFactory : error : conf file not find");
        }

        properties = new Properties();
        properties.load(inputStream);

        if (properties.isEmpty()) {
            logger.info("CommandFactory : CommandFactory : error : conf file is empty");
        }

        logger.info("CommandFactory : CommandFactory : end");
    }

    public static CommandFactory getInstance() {
        logger.info("CommandFactory : getInstance : begin");

        try {
            if (factory == null) {
                factory = new CommandFactory();
            }
        } catch (Exception e) {
            logger.info("CommandFactory : getInstance : error : " + e.getMessage());
            e.printStackTrace();
        }

        logger.info("CommandFactory : getInstance : end");
        return factory;
    }

    public Command createCommand (String commandName) throws Exception {
        logger.info("CommandFactory : createCommand : begin");

        String commandClassName = properties.getProperty(commandName);

        if (commandClassName == null) {
            logger.info("CommandFactory : createCommand : error : command does not exist - " + commandName);
            throw new NoCommandException("CommandFactory: command does not exist " + commandName);
        }

        logger.info("CommandFactory : createCommand : commandName = " + commandName);

        @SuppressWarnings("unchecked")
        Class<Command> commandClass = (Class<Command>) Class.forName(commandClassName);
        Constructor<Command> commandConstructor = commandClass.getDeclaredConstructor();

        logger.info("CommandFactory : createCommand : end");
        return commandConstructor.newInstance();
    }
}
