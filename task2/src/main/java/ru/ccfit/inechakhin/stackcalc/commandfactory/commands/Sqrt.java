package ru.ccfit.inechakhin.stackcalc.commandfactory.commands;

import ru.ccfit.inechakhin.stackcalc.commandfactory.Command;
import ru.ccfit.inechakhin.stackcalc.exceptions.NotEnoughElemException;
import ru.ccfit.inechakhin.stackcalc.exceptions.NullStackException;
import ru.ccfit.inechakhin.stackcalc.exceptions.RootOfNegException;

import java.util.HashMap;
import java.util.Stack;
import java.util.logging.Logger;

public class Sqrt implements Command {
    private static final Logger logger = Logger.getLogger(Sqrt.class.getName());

    @Override
    public void execute (HashMap<String, Double> map, Stack<Double> stack, String param) {
        logger.info("Sqrt : execute : begin");

        if (stack == null) {
            logger.info("Sqrt : execute : error : stack is not initialized");
            throw new NullStackException("Sqrt: stack is not initialized");
        }
        if (stack.size() < 1) {
            logger.info("Sqrt : execute : error : stack has less than 1 elements");
            throw new NotEnoughElemException("Sqrt: stack has less than 1 elements");
        }

        Double num = stack.lastElement();
        stack.pop();

        logger.info("Sqrt : execute : num = " + num.toString());

        if (num < 0) {
            logger.info("Sqrt : execute : error : root of negative number");
            throw new RootOfNegException("Sqrt: root of negative number");
        }

        stack.push(Math.sqrt(num));

        logger.info("Sqrt : execute : end");
    }
}
