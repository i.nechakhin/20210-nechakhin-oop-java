package ru.ccfit.inechakhin.stackcalc.commandfactory.commands;

import ru.ccfit.inechakhin.stackcalc.commandfactory.Command;

import java.util.HashMap;
import java.util.Stack;

public class Comment implements Command {
    @Override
    public void execute (HashMap<String, Double> map, Stack<Double> stack, String param) {}
}
