package ru.ccfit.inechakhin.stackcalc;

import java.util.logging.Logger;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main (String[] args) {
        logger.info("Main : main : begin");

        try {
            logger.info("Main : main : creating calculator");
            Calculator calculator = new Calculator();
            logger.info("Main : main : calculator was created");
            if (args.length == 0) {
                calculator.calculate();
            } else {
                calculator.calculate(args[0]);
            }
        } catch (Exception e) {
            logger.info("Main : main : error : " + e.getMessage());
            System.out.println(e.getMessage());
        }

        logger.info("Main : main : end");
    }
}
