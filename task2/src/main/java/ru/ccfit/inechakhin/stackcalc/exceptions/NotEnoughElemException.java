package ru.ccfit.inechakhin.stackcalc.exceptions;

public class NotEnoughElemException extends StackCalcException {
    public NotEnoughElemException (String message) {
        super(message);
    }
}
