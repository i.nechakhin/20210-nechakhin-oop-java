package ru.ccfit.inechakhin.stackcalc.exceptions;

public class DivByZeroException extends StackCalcException {
    public DivByZeroException (String message) {
        super(message);
    }
}
